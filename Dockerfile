FROM node:latest

WORKDIR /app

COPY . /app/

RUN npm install --omit=dev

EXPOSE 3000

CMD [ "npm", "start" ]